import java.util.Random;

public class PasswordGenerator {
    private int passwordLength;

    public PasswordGenerator(int passwordLength) {
        this.passwordLength = passwordLength;
    }

    public String generateWeakPassword() {
        Random rand = new Random();
        StringBuilder weakPassword = new StringBuilder(this.passwordLength);

        for (int i = 0; i < this.passwordLength; i++) {
            char letter = (char) (rand.nextInt(26) + 'a');
            weakPassword.append(letter);
        }
        return weakPassword.toString();
    }

    public String generateStrongPassword() {
        Random rand = new Random();
        StringBuilder strongPassword = new StringBuilder(this.passwordLength * 2);

        for (int i = 0; i < this.passwordLength * 2; i++) {

            for (; i < this.passwordLength * 2; i++) {
                char capLetter = (char) (rand.nextInt(26) + 'a');
                strongPassword.append(capLetter);
            }
            for (; i < this.passwordLength * 2; i++) {
                char lowLetter = (char) (rand.nextInt(26) + 'A');
                strongPassword.append(lowLetter);

            }
            for (; i < this.passwordLength * 2; i++) {
                int randNum = (rand.nextInt(9));
                strongPassword.append(randNum);
            }
        }
        return strongPassword.toString();
    }
}
