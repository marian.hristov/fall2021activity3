import org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.params.shadow.com.univocity.parsers.annotations.Validate;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;

public class LoginTest {

    @Test
    public void validateTestFalse() {

        User[] users = new User[4];
        users[0] = new User("gui", "1234", UserType.REGULAR);
        users[1] = new User("mauri", "5678", UserType.REGULAR);
        users[2] = new User("marian", "4321", UserType.ADMIN);
        users[3] = new User("boo", "ah", UserType.REGULAR);

        Login login = new Login(users);
        assertEquals(login.validate("gui", "abcd", true), false);
    }

    @Test
    public void validateTestTrueAdmin() {

        User[] users = new User[4];
        users[0] = new User("gui", "1234", UserType.REGULAR);
        users[1] = new User("mauri", "5678", UserType.REGULAR);
        users[2] = new User("marian", "4321", UserType.ADMIN);
        users[3] = new User("boo", "ah", UserType.REGULAR);

        Login login = new Login(users);
        assertEquals(login.validate("marian", "4321", true), true);
    }

    @Test
    public void validateTestTrueRegular() {

        User[] users = new User[4];
        users[0] = new User("gui", "1234", UserType.REGULAR);
        users[1] = new User("mauri", "5678", UserType.REGULAR);
        users[2] = new User("marian", "4321", UserType.ADMIN);
        users[3] = new User("boo", "ah", UserType.REGULAR);

        Login login = new Login(users);
        assertEquals(login.validate("mauri", "5678", false), true);
    }

}
