public class Login {
    private User[] users;

    public Login(User[] givenUsers) {
        this.users = givenUsers;
    }

    public boolean validate(String username, String password, boolean admin) {
        for (User us : this.users) {
            if (!admin && (us.getUsername().equals(username) && us.getPassword().equals(password))) {
                return true;
            } else if (admin && (us.getUsername().equals(username) && us.getPassword().equals(password)
                    && us.getType() == UserType.ADMIN)) {
                return true;
            }
        }
        return false;
    }
}
