import org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class PasswordGeneratorTest {

    @Test
    public void generateWeakPasswordTest() {
        PasswordGenerator pw = new PasswordGenerator(8);
        pw.generateStrongPassword();
        pw.generateWeakPassword();
    }
}